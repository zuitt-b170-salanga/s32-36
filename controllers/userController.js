/* s32-36-controllers userController.js */

// set up dependencies
const User = require("../models/User.js")
const Course = require("../models/Course.js")
const auth = require ("../auth.js")
const bcrypt = require("bcrypt") // used to encrypt user passwords

// check if the email exists
/*
	1. check  for the email in the db
	2. send the result as a response (w/ error handling)
*/
/*
	it is conventional for the devs to use Boolean in sending return responses esp with the backend application
*/


module.exports.addCourse = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin === false) {
            return "You are not an admin"
        } else {
            let newCourse = new Course({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            //Saves the created object to the database
            return newCourse.save().then((course, error) => {
                //Course creation failed
                if(error) {
                    return false
                } else {
                    //course creation successful
                    return "Course creation successful"
                }
            })
        }
        
    });    
}



module.exports.checkEmail = (requestBody) => {
	return User.find({ email: requestBody.email }).then((result, error) => {
		if (error) {
			console.log(error)
			return false
		}else{
			if (result.length > 0) {
				// return result
				return true
			} else {
				// return res.send("email does not exist")
				return false
			}
		}
	})
}

/*
	USER REGISTRATION

	1: create a new User w/ the info from the requestBody
	2. make sure that the pw is encrypted
	3. save the new user
*/

module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		age: reqBody.age,
		gender: reqBody.gender,
		email: reqBody.email,
		// hashSync is a function of bcrypt that encrypts the pw
			// 10 the number of rounds/times it runs the algorithm to the reqBody.password
				// max 72 implementations
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	})
	return newUser.save().then((saved, error) => {
		if (error){
			console.log(error)
			return false
		} else {
			return true
		}
	})
}

// USER LOGIN
/*
	1. find if the email is existing in the database
	2. check if the pw is correct
*/

module.exports.userLogin = (reqBody) => {
	return User.findOne( { email: reqBody.email } ).then(result => {
		if (result === null) {
			return false
		} else {
			// compareSync function - used to compare a non-encrypted password to an encrypted password 
				//and retuns a Boolean reasponse depending on the result
			/*
			What should we do after the comparison?
				true - a token should be created since the user is existing and the password is correct
				false - the passwords do not match, thus a token should not be created
			*/
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result.toObject())}
				// auth - imported auth.js
				// createAccessToken - function inside the auth.js to create access token
				// .toObject - transforms the User into an Object that can be used by our createAccessToken
			} else {
				return false
			}
		}
	})
}

/*
	miniActivity
		create getProfile function inside the userController
			1. find the id of the user using "data" as parameters
			2. if it does not exist, return false,
			3. otherwise, return the details (stretch - make the pw invisible)
*/

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		// if there is no user found...
		if(result === null) {
				return false
			// if a user is found...	
			} else {
				result.password = "";
				return result
		}
	})
}

// ======================================
// ======================================
// get all users
module.exports.getAllUsers = () => {
	return User.find( {} ).then((result, error) => {
		if (error) {
			return false
		}else{
			return result
		}
	})
}
// ======================================
// ======================================
// enroll user

/*
	1. find the user/document in the database
	2. add the courseId to the user's enrollment array
	3. save the document in the database
*/

module.exports.enroll = async (data) => {
	//
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// adding the courseId to the user's enrollment array
		user.enrollments.push({courseId:data.courseId})

		// saving in the database
		return user.save().then((user, err) => {
			if (err) {
				return false
			} else {
				return true
			}
		})
	})

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		// adding the userId to the course's enrollees array
		course.enrollees.push({userId:data.userId})

		// saving in the database
		return course.save().then((course, err) => {
			if (err) {
				return false
			} else {
				return true
			}
		})
	})
	
	if (isUserUpdated && isCourseUpdated){
		return true
	}
	else{
		return false
	}
}



/*router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
		}	

		userController.enroll(data).then(result => res.send(result))
	})*/

/*
	user another await keyword to update the enrollees array in the course collection

		find the courseId from the requestBody
		push the userId of the enrollee in the enrollees array of the course
		update the document in the database 

		if both pushing are successful, return true
		if both pushing are not successful, return false
*/


/*
Deploying a server

1. register in Heroku (https://signup.heroku.com/login)
2. check if you have heroku in your device
3. create a "Procfile" file
		- it should have P
		- it should not have any file extensions
		- inserting the text "web: node app"
4. login your heroku through the git bash/terminal
		- "heroku login"
				press any key
				log in using your heroku account
				there should be confirmation
5. heroku create to create/deploy your server app to a host in the internet
6. git push heroku master to push the updates in heroku
*/

	
/*
	s32-36
	heroku --v
*/

// ======================================
// ======================================
















































// module.exports.enroll = async (data) => {

// 	// let's give this a shot
	// let isCourseActive = await Course.find({isActive: true}).then ((result, error) => {
	// 	if (error) {
	// 		return false
	// 	} else {
	// 		return true
	// 		}
	// })
// 	// let's give this a shot


// 	let isUserUpdated = await User.findById(data.userId).then(user => {
// 		// adding the courseId to the user's enrollment array
// 		user.enrollments.push({courseId:data.courseId})

// 		// saving in the database
// 		return user.save().then((user, err) => {
// 			if (err) {
// 				return false
// 			} else {
// 				return true
// 			}
// 		})
// 	})

	

// 	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
// 		// adding the userId to the course's enrollees array
// 		course.enrollees.push({userId:data.userId})

// 		// saving in the database
// 		return course.save().then((course, err) => {
// 			if (err) {
// 				return false
// 			} else {
// 				return true
// 			}
// 		})
// 	})

	

// 	// let isCourseUpdated = await Course.findById(data.courseId).then(course => {
// 	// 	// adding the userId to the course's enrollees array
// 	// 	course.enrollees.push({userId:data.userId})

// 	// 	// saving in the database
// 	// 	return course.save().then((course, err) => {
// 	// 		if (err) {
// 	// 			return false
// 	// 		} 

// 	// 		else {
// 	// 			return true
// 	// 		}

// 	// 		/* we'll see if it works */
// 	// 		// else {
// 	// 		// 	return Course.find({isActive: true}).then ((result, error) => {
// 	// 		// 		if (error) {
// 	// 		// 			console.log(error)
// 	// 		// 		} else {
// 	// 		// 			return result
// 	// 		// 		}
// 	// 		// 	})
// 	// 		// }
// 	// 		/* we'll see if it works */

// 	// 	})
// 	// })


// 	// let's give this a shot
	// if (isCourseActive && isUserUpdated && isCourseUpdated){
// 	// let's give this a shot
// 	//if (isUserUpdated && isCourseUpdated){
// 		return true
// 	}
// 	else{
// 		return false
// 	}
// }


