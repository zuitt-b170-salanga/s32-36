/* s32-36-controllers courseController.js */

const User = require("../models/User.js")
const Course = require("../models/Course.js")
const auth = require ("../auth.js")
const bcrypt = require("bcrypt") 


// ============================================

// /*
// Business Logic (courseController) 
// ACTIVITY s34     
// 1. find the user in the database, find out if the user is admin 
// 2. if the user is not an admin, send the response "You are not an admin" 
// 3. if the user is an admin, create the new Course object 
// -save the object 
// -if there are errors, return false 
// -if there are no errors, return "Course created sucessfully"
// */


// module.exports.addCourse = (reqBody,userData) => {
	

// 	return User.findById(userData.userid).then(result => {

// 		if(userData.isAdmin===false) {
// 				return "You are not an admin"	
// 			} else {
// 				let newCourse = new Course({
// 					name:reqBody.name,
// 					description:reqBody.description,
// 					price:reqBody.price
// 				})

// 				return newCourse.save().then((course, error) => {
//  			if (error) {
//  				return false
//  			} else {
//  			return "Course creation successful"
//  		}
//  	})
//  }





// =============================================

/*
ACTIVITY s34
	1. find the user in the database
		find out if the user is admin

	2. if the user is not an admin, send the response "You are not an admin"

	3. if the user is an admin, create the new Course object
			-save the object
				-if there are errors, return false
				-if there are no errors, return "Course created sucessfully"
*/

module.exports.addCourse = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin === false) {
            return "You are not an admin"
        } else {
            let newCourse = new Course({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            //Saves the created object to the database
            return newCourse.save().then((course, error) => {
                //Course creation failed
                if(error) {
                    return false
                } else {
                    //course creation successful
                    return "Course creation successful"
                }
            })
        }
        
    });    
}

// retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find( {} ).then((result, error) => {
		if (error) {
			return false
		}else{
			return result
		}
	})
}

/*
	using the course id (params) in the url, retrieve a course using a get request
		send the code snippet in the batch google chat
*/

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams).then(result => {
		return result
	})
}

// retrieve all active courses
module.exports.getActive = () => {
	return Course.find( { isActive: true } ).then((result,error) => {
		if (error) {
			console.log(error)
		}else{
			return result
		}
	})
}

// ======================================
// ======================================
// update a course
module.exports.updateCourse = ( reqParams, reqBody ) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	// findByIdAndUpdate - looks for the id of the document (first parameter) and updates it (content of the second parameter)
		//the server will be looking for the id of the reqParams.courseId in the database and updates the document through the content of the object updatedCourse 
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((result, error) => {
		if (error) {
			return false
		} else	{
			return true
		}
	})
}

// ======================================
// ======================================
// archive a course
/*
	controllers business logic:
	1. create an updateCourse object with the content from the reqBody
			- reqBody should have the isActive status of the course to be set to false
	2. find the course by its id and update with the updateCourse object using the findByIdAndUpdate
			handle the errors that may arise
				error/s - false
				no errors - true
*/

module.exports.archiveCourse = ( reqParams, reqBody ) => {
	let updatedCourse = {
		isActive: reqBody.isActive
	}
	
	/*
		If you do not want to require any request body:
		module.exports.archivedCourse=(reqParams) => {
			let updatedCourse = {
				isActive: false
			}
	*/
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((result, error) => {
		if (error) {
			return false
		} else	{
			return true
		}
	})
}

/*
	user another await keyword to update the enrollees array in the course collection

		find the courseId from the requestBody
		push the userId of the enrollee in the enrollees array of the course
		update the document in the database 

		if both pushing are successful, return true
		if both pushing are not successful, return false
*/

/*router.put("/:courseId/addEnrollee", auth.verify, (req, res) => {
	let data = {
			userId: auth.decode(req.headers.authorization).id,
			courseId: req.body.courseId
		}	
		userController.addEnrollee(data).then(result => (res.send))
})*/

module.exports.addEnrollee = async (data) => {
	let isUserAdded = await 
}
// ======================================
// ======================================

/*
	1. find the user/document in the database
	2. add the courseId to the user's enrollment array
	3. save the document in the database
*/


/*
module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// adding the courseId to the user's enrollment array
		user.enrollments.push({courseId:data.courseId})

		// saving in the database
		return user.save().then((user, err) => {
			if (err) {
				return false
			} else {
				return true
			}
		})
	})
	if (isUserUpdated){
		return true
	}
	else{
		return false
	}
}
*/



























