/* s32-36-routes courseRoutes.js */

const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const courseController = require("../controllers/courseController.js")

// =====================================

// /*
// s34 business logic (courseRoutes) 
// ACTIVITY 

// create a route that will let an admin perform addCourse function in the courseController 

// verify that the user is logged in 

// decode the token for that user 

// use the id, isAdmin, and request body to perform the function in courseController 
// the id and isAdmin are parts of an object
// */

// // ok
// router.post("/", auth.verify, (req, res) => {
//  	const userData = auth.decode(req.headers.authorization)  // userData would now contain an obj
//  		//that has the token payload(id,email,isAdmin info of the user)
//  	courseController.addCourse(req.body,userData).then(resultFromController => res.send(
// 		resultFromController))
// })

// // prev code
// // router.post("/courses", auth.verify, (req, res) => {
// //  	const userData = auth.decode(req.headers.authorization)
// //  	courseController.addCourse({userId:userData.id}).then(resultFromController => res.send(
// // 		resultFromController))
// // })


// =====================================


/*
ACTIVITY
create a route that will let an admin perform addCourse function in the courseController
	verify that the user is logged in
	decode the token for that user
	use the id, isAdmin, and request body to perform the function in courseController
		the id and isAdmin are parts of an object 
*/
router.post("/", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization) //userData would now contain an object that has the token payload (id, email, isAdmin information of the user)
    courseController.addCourse(req.body, userData).then(resultFromController => res.send(resultFromController))
})




/*	ecommerce websites */

/*
create a route that will retrieve all of our products/courses
	will not require login/register functions
*/
router.get("/", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})

/*
	in getting all of the documents, in case we need multiple of them, place the route with the criteria to the find method first before getting the one with params
*/
// retrieve all active courses
router.get("/active", (req, res) => {
	courseController.getActive().then(resultFromController => res.send(resultFromController))
})

/*
miniactivty
	create a route that will retrieve a course hint: params
		will not require login/register from the users
*/
// retrieve a course
router.get("/:courseId",  (req, res) => {
	console.log(req.params.courseId);
	courseController.getCourse(req.params.courseId).then(result => res.send(result))
})

// ======================================
// ======================================
// update a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(result => res.send(result))
})


// ======================================
// ======================================
/*
delete is never a norm in databases
*/
/*
	routes business logic: 
	use /archiveCourse and send a PUT request to archive a course by changing the active status
*/

// archive a course
router.put("/:courseId/archiveCourse", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params, req.body).then(result => res.send(result))
})

/*
	if you do not want to require any rqst body
	courseController.archiveCourse(req.params).then(result => res.send(result))
*/
// ======================================
// ======================================

	/*router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
		}	

		userController.enroll(data).then(result => res.send(result))
	})*/

/*
	user another await keyword to update the enrollees array in the course collection

		find the courseId from the requestBody
		push the userId of the enrollee in the enrollees array of the course
		update the document in the database 

		if both pushing are successful, return true
		if both pushing are not successful, return false
*/

/*
delete later ----->
router.put("/:courseId/addEnrollee", auth.verify, (req, res) => {
	let data = {
			userId: auth.decode(req.headers.authorization).id,
			courseId: req.body.courseId
		}	
		userController.addEnrollee(data).then(result => (res.send))
})*/


// ==================================
// ==================================

module.exports = router









