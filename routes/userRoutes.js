/* s32-36-routes userRoutes.js */

const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const userController = require("../controllers/userController.js")

/*
	Mini Activity
	create a function w/ the "/checkEmail" endpoint that will be able to use the 
		checkEmail function in the userController
	reminder that we have a parameter needed in the checkEmail function & that is 
		data fr request body
*/


// ********************************************
// checking of email  ----->
router.post("/checkEmail", (req, res)=> {
	userController.checkEmail(req.body).then(resultFromController => res.send(resultFromController))
})
// ********************************************

// user registration  ----->
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})


// user login ----->
router.post("/login", (req,res) => {
	userController.userLogin(req.body).then(resultFromController => res.send(resultFromController))
})


// auth.verify -----> ensures that a user is logged in before proceeding to the nxt part of the code; 
	//this is the verify function inside the auth.js 
router.get("/details", auth.verify, (req,res) => {
	// decode - decrypts the token inside the authorization (w/c is in the headers of the rqst)
	// req.headers.authorization - contains the token that was created for the user
	const userData = auth.decode(req.headers.authorization)
	userController.getProfile({userId:userData.id}).then(resultFromController => res.send(
		resultFromController))

})

// ======================================
// ======================================
// get all users

router.get("/", (req, res) => {
	userController.getAllUsers().then(resultFromController => res.send(resultFromController))
})

// ======================================
// ======================================
// enroll user


// router.post("/:courseId/enroll", auth.verify, (req, res) => {
// 	userController.enrollCourse(req.params, req.body).then(result => res.send(result))
// })


// Enrol user
// POST
// http://localhost:4000/api/users/626a6b136e1b05107430bff6/enroll
// {
//     "courseId":"626e123594fcfa70aa5c0f8a"
// }
// output:
// true

// ======================================
// ======================================

/*
	miniactivity
		create a post rqst under the '/enroll' endpoint that would allow the use of "enroll" function 
			in the userController
		check if the user is logged in
		create a variable & store an obj w/ userId & courseId as fields
			userId - decoded token of the user
			courseId - the course id present in the request body

		user the variable as a parameter of the enrol function

		send a screenshot of your codes in our batch google chat
*/

	router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
		}	

		userController.enroll(data).then(result => res.send(result))
	})

/*
	user another await keyword to update the enrollees array in the course collection

		find the courseId from the requestBody
		push the userId of the enrollee in the enrollees array of the course
		update the document in the database 

		if both pushing are successful, return true
		if both pushing are not successful, return false
*/

	
// ======================================
// ======================================


module.exports = router